angular.module('d3test', ['angular-meteor']);

angular.module('d3test').controller('TestCtrl', function ($meteor, $scope, $window, $timeout) {

  var self = this;

  this.loading = false;
  this.set = 'A';
  this.n = 100;
  this.skip = 0;
  this.interpolation = 'linear';

  var svg;
  var x = d3.scale.linear().range([0, 500]).domain([0, 1]);
  var y = d3.scale.linear().range([500, 0]).domain([0, 1]);
  var line = d3.svg.line()
    .x(function(d) { return x(d.x); })
    .y(function(d) { return y(d.y); });

  $meteor.autorun($scope, function() {
    self.loading = true;
    $meteor.subscribe('data', $scope.getReactively('test.set'), +$scope.getReactively('test.n'), +$scope.getReactively('test.skip')).then(function() {
      self.loading = false;
    });
  });

  this.data = $scope.$meteorCollection(Data);

  var renderData = function() {
    var oldData = svg.select('path').datum() || [];
    
    svg.select('path').datum(oldData.map(function(d) { return _.extend({}, d, {y: y.domain()[0]}); })).transition().duration(500).attr('d', line).each('end', function() {
      line.interpolate(self.interpolation);
      x.domain(d3.extent(self.data, function(d) { return d.x; }));
      y.domain(d3.extent(self.data, function(d) { return d.y; }));

      svg.select('path').datum(self.data).transition().duration(500).attr('d', line);
    });

    // var circles = svg.selectAll('circle').data(self.data, function(d) { return d._id; });
    // circles.enter().append('circle')
    //   .attr('cx', function(d) { return x(d.x); })
    //   .attr('cy', 500)
    //   .attr('r', 0);

    // circles.transition().duration(500)
    //   .attr('cx', function(d) { return x(d.x); })
    //   .attr('cy', function(d) { return y(d.y); })
    //   .attr('r', 5);

    // circles.exit().transition().duration(500)
    //   .attr('cy', 500)
    //   .attr('r', 0)
    //   .remove();
  };

  $timeout(function() {
    svg = d3.select(document).select('svg');
    console.log('svg', svg);
    svg.attr('width', 500).attr('height', 500);
    svg.append('path').attr('class', 'line').attr('fill', 'none');

    $scope.$watch('test.data', function() {
      console.log('changed, rerendering');
      renderData();
    }, true);
    $scope.$watch('test.interpolation', function() {
      renderData();
    });
  }, 0);

  $window.blabla = this.data;

});
