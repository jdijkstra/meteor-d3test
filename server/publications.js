Meteor.publish('data', function(set, n, skip) {
  return Data.find({set: set}, {limit: n, sort: {x: 1}, skip: skip});
});